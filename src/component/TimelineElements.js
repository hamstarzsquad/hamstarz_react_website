let timelineElements = [
    {
      id: 1,
      title: "$15,000 ETH Prize Giveaway",
      description:
        "When the 1337 Hamstar is minted, we will do a $15,000 ETH giveaway to one LUCKY Hamstar holder! The more Hamstarz you hold the higher your chance to win!",
      date: "15% MINTED",
      icon: "work",
    },
    {
      id: 2,
      title: "1-of-1 NFT Giveaways",
      description:
        "We will give away 10 one-of-one Hamstarz NFTs to 10 lucky holders, personalized to their request. Thank you so much for believing in us!",
      date: "20% MINTED",
      icon: "work",
    },
    {
      id: 3,
      title: "Hamstarzland Grand Opening",
      description:
        " We have been hard at work building our first Hamstarzland theme park in collaboration with The Nemesis metaverse platform, as seen in our Discord sneak peeks. We will go live with our grand opening and host our first series of community mini-game tournaments to celebrate!",
      date: "30% MINTED",
      icon: "work",
    },
    {
      id: 4,
      title: "Charity Donation Giveaway",
      description:
        "We will host our first charity donation giveaway live inside our theme park's interactive streaming theatre. With the help of the community, we will decide on a charity, or charities, to donate $20,000 to support animal rescue and wellfare. ",
      date: "40% MINTED",
      icon: "school",
    },
    {
      id: 5,
      title: "Free Companion NFT",
      description:
        "Hamstarz NFT holders will receive a free companion NFT, which will provide unique benefits when paired with Hamstarz come breeding season. Hamstarz are quite social, and bond with not only other Hamstarz, but also with other species we have not yet encountered!",
      date: "60% MINTED",
      icon: "school",
    },
    {
      id: 6,
      title: "Breeding Adorable Babies",
      description:
        "Holders of both a Hamstarz and a companion are able to breed and create babies. Depending on the type of companion, we may even see the birth of some entirely new species!",
      date: "80% MINTED",
      icon: "school",
    },
    {
        id: 7,
        title: "Merch Store",
        description:
            "An exclusive Hamstarz merch store will drop, featuring clothing and stationary for your everyday needs. We are super excited for the opportunity to spice things up in the human world for our community.",
        date: "90% MINTED",
        icon: "school",
    },
    {
        id: 8,
        title: "Metaverse Expansion: NetVRk and Sandbox",
        description:
            "Our team has acquired 5 Large Lands in NetVRk, totalling over 50,000 sq meterse (12.3 acres)! Hamstarzland will expand to NetVRk, Sandbox, and Voxels on our mission to be the Disneyland of the metaverses.",
        date: "100% MINTED",
        icon: "school",
    },
    {
        id: 9,
        title: "$HAMMY Token",
        description:
            "We will integrate a fully compliant native utility token. Earn $HAMMY by holding your Hamstarz and participating in our metaverse gaming experiences and competitions. $HAMMY token will be your key to unlocking exclusive metaverse experiences and future planned NFT drops.",
        date: "Q1 2022",
        icon: "school",
    },
  ];
  
  export default timelineElements;
  