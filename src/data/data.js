import twitter from "../asset/twitter.png";
import discord from "../asset/discord.png";
import twitter_white from "../asset/twitter-white.png"
import discord_white from "../asset/discord-white.png"
import hammy1 from "../asset/Hammy1.png"
import hammy2 from "../asset/Hammy2.png"
import hammy3 from "../asset/Hammy3.png"
import sea_black from "../asset/sea-black.png"
import sea from "../asset/sea.png"

export const story = {
    Story_Description1: "The year is 800 AH (after Hamstarzland), today is the special 800th anniversary of Hamstarzland's Grand Opening. You are among the thousands of excited hammys across Hammyville rushing to the capital city. After hours of waiting in traffic, you have finally arrived at the grand entrance of the park. You just need to purchase an admission ticket to start your magical adventure at Hamstarzland.",
}

export const faq = [
    { question: "What is Hamstarz Squad?", answer: "Hamstarz Squad is a collection of 8,888 lovable hamster NFTs living on the Ethereum Blockchain. Each hamster is uniquely generated from a random combination of over 300 traits, and is your key to our exclusive metaverse theme parks with gaming events and contests with ETH and NFT rewards.", key: '1' },
    { question: "How do I join the Hamstarz Squad?", answer: "Once minting is live, you can connect your Metamask wallet and adopt up to 20 Hamstarz in one transaction.", key: '2' },
    { question: "When is the launch?", answer: "Launch is Tuesday 12/7 @ 6PM EST (11PM GMT).", key: '3' },
    { question: "How much will it cost to mint each Hamstar?", answer: "0.045 ETH + gas (~$20-30). We recommend having ~0.06 ETH total in your wallet to cover gas", key: '4' },
    { question: "How many Hamstarz can I mint in per wallet?", answer: "Max 20 Hamstarz per transaction. No limit per wallet.", key: '5' },
    { question: "Where can I see the roadmap?", answer: "High level roadmap is posted above under 'Roadmap'. Always check discord for the latest information and updates.", key: '6' }
]

export const team = [
    { name: "Lily", image: hammy1, desc: "Artist and creative lead. Former Twitch creative partner and DPS Moira main.", fb: "https://www.facebook.com/Lillipie102", key: "1" },
    { name: "Eric", image: hammy2, desc: "Developer. Former elite WoW priest and no-aim-needed Reinhardt main.", key: "2" },
    { name: "Cenk", image: hammy3, desc: "Marketing. NFT degen and OG Preme & Streetwear collector.", key: "3" },
]

export const social = [
    /* for the footer, normal colors */
    { name: "Twitter", link: "https://twitter.com/hamstarznft", image: twitter, key: "1" },
    { name: "Discord", link: "https://discord.gg/hamstarznft", image: discord, key: "2" },
    /*{ name: "Instagram", link: "https://www.instagram.com/", image: instagram, key: "3" },*/
   /* { name: "Sea", link: "https://opensea.io/collection/", image: sea_black, key: "4" },*/
]

export const home_social = [
    /* for header area */
    { name: "Twitter", link: "https://twitter.com/hamstarznft", image: twitter_white, key: "1" },
    { name: "Discord", link: "https://discord.gg/hamstarznft", image: discord_white, key: "2" },
    /*{ name: "Instagram", link: "https://www.instagram.com/", image: insta_white, key: "3" },*/
    /*{ name: "Sea", link: "https://opensea.io/collection/", image: sea, key: "4" },*/
]

export const support = "contact@hamstarznft.com"